#!/bin/bash

rm amoslinux-3rdparty-repo*
echo "repo-add"
repo-add -n -R amoslinux-3rdparty-repo.db.tar.gz *.pkg.tar.zst

sleep 1
rm amoslinux-3rdparty-repo.db
rm amoslinux-3rdparty-repo.files

mv amoslinux-3rdparty-repo.db.tar.gz amoslinux-3rdparty-repo.db
mv amoslinux-3rdparty-repo.files.tar.gz amoslinux-3rdparty-repo.files

echo "#################################################"
echo "Repo Updated !!"
echo "#################################################"